{
    "meta": {
        "name": "TA-sophos-central-addon-for-splunk",
        "displayName": "Sophos Central Addon for Splunk",
        "version": "1.0.0",
        "apiVersion": "3.0.0",
        "restRoot": "TA_sophos_central_addon_for_splunk"
    },
    "pages": {
        "configuration": {
            "title": "Configuration",
            "description": "Set up your add-on",
            "tabs": [
                {
                    "name": "proxy",
                    "title": "Proxy",
                    "entity": [
                        {
                            "field": "proxy_enabled",
                            "label": "Enable",
                            "type": "checkbox"
                        },
                        {
                            "field": "proxy_type",
                            "label": "Proxy Type",
                            "type": "singleSelect",
                            "options": {
                                "disableSearch": true,
                                "autoCompleteFields": [
                                    {
                                        "label": "http",
                                        "value": "http"
                                    },
                                    {
                                        "label": "socks4",
                                        "value": "socks4"
                                    },
                                    {
                                        "label": "socks5",
                                        "value": "socks5"
                                    }
                                ]
                            },
                            "defaultValue": "http"
                        },
                        {
                            "field": "proxy_url",
                            "label": "Host",
                            "type": "text",
                            "validators": [
                                {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 4096,
                                    "errorMsg": "Max host length is 4096"
                                }
                            ]
                        },
                        {
                            "field": "proxy_port",
                            "label": "Port",
                            "type": "text",
                            "validators": [
                                {
                                    "type": "number",
                                    "range": [
                                        1,
                                        65535
                                    ]
                                }
                            ]
                        },
                        {
                            "field": "proxy_username",
                            "label": "Username",
                            "type": "text",
                            "validators": [
                                {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 50,
                                    "errorMsg": "Max length of username is 50"
                                }
                            ]
                        },
                        {
                            "field": "proxy_password",
                            "label": "Password",
                            "type": "text",
                            "encrypted": true,
                            "validators": [
                                {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 8192,
                                    "errorMsg": "Max length of password is 8192"
                                }
                            ]
                        },
                        {
                            "field": "proxy_rdns",
                            "label": "Remote DNS resolution",
                            "type": "checkbox"
                        }
                    ],
                    "options": {
                        "saveValidator": "function(formData) { if(!formData.proxy_enabled || formData.proxy_enabled === '0') {return true; } if(!formData.proxy_url) { return 'Proxy Host can not be empty'; } if(!formData.proxy_port) { return 'Proxy Port can not be empty'; } return true; }"
                    }
                },
                {
                    "name": "logging",
                    "title": "Logging",
                    "entity": [
                        {
                            "field": "loglevel",
                            "label": "Log level",
                            "type": "singleSelect",
                            "options": {
                                "disableSearch": true,
                                "autoCompleteFields": [
                                    {
                                        "label": "DEBUG",
                                        "value": "DEBUG"
                                    },
                                    {
                                        "label": "INFO",
                                        "value": "INFO"
                                    },
                                    {
                                        "label": "WARNING",
                                        "value": "WARNING"
                                    },
                                    {
                                        "label": "ERROR",
                                        "value": "ERROR"
                                    },
                                    {
                                        "label": "CRITICAL",
                                        "value": "CRITICAL"
                                    }
                                ]
                            },
                            "defaultValue": "INFO"
                        }
                    ]
                },
                {
                    "name": "additional_parameters",
                    "title": "Add-on Settings",
                    "entity": [
                        {
                            "field": "client_id",
                            "label": "Client ID",
                            "type": "text",
                            "help": "Enter Sophos Client ID.",
                            "required": true,
                            "defaultValue": "",
                            "validators": [
                                {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 8192,
                                    "errorMsg": "Max length of text input is 8192"
                                }
                            ]
                        },
                        {
                            "field": "client_secret",
                            "label": "Client Secret",
                            "type": "text",
                            "help": "Enter Sophos Client Secret.",
                            "required": true,
                            "defaultValue": "",
                            "encrypted": true,
                            "validators": [
                                {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 8192,
                                    "errorMsg": "Max length of password is 8192"
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        "inputs": {
            "title": "Inputs",
            "description": "Manage your data inputs",
            "table": {
                "header": [
                    {
                        "field": "name",
                        "label": "Name"
                    },
                    {
                        "field": "interval",
                        "label": "Interval"
                    },
                    {
                        "field": "index",
                        "label": "Index"
                    },
                    {
                        "field": "disabled",
                        "label": "Status"
                    }
                ],
                "moreInfo": [
                    {
                        "field": "name",
                        "label": "Name"
                    },
                    {
                        "field": "interval",
                        "label": "Interval"
                    },
                    {
                        "field": "index",
                        "label": "Index"
                    },
                    {
                        "field": "disabled",
                        "label": "Status"
                    },
                    {
                        "field": "page_size",
                        "label": "Page Size"
                    },
                    {
                        "field": "from",
                        "label": "From"
                    },
                    {
                        "field": "to",
                        "label": "To"
                    }
                ],
                "actions": [
                    "edit",
                    "enable",
                    "delete",
                    "clone"
                ]
            },
            "services": [
                {
                    "name": "sophos_alert_input",
                    "title": "Sophos Alert Input",
                    "entity": [
                        {
                            "field": "name",
                            "label": "Name",
                            "type": "text",
                            "help": "Enter a unique name for the data input",
                            "required": true,
                            "validators": [
                                {
                                    "type": "regex",
                                    "pattern": "^[a-zA-Z]\\w*$",
                                    "errorMsg": "Input Name must start with a letter and followed by alphabetic letters, digits or underscores."
                                },
                                {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 100,
                                    "errorMsg": "Length of input name should be between 1 and 100"
                                }
                            ]
                        },
                        {
                            "field": "interval",
                            "label": "Interval",
                            "type": "text",
                            "required": true,
                            "help": "Time interval of input in seconds.",
                            "validators": [
                                {
                                    "type": "regex",
                                    "pattern": "^\\-[1-9]\\d*$|^\\d*$",
                                    "errorMsg": "Interval must be an integer."
                                }
                            ]
                        },
                        {
                            "field": "index",
                            "label": "Index",
                            "type": "singleSelect",
                            "defaultValue": "default",
                            "options": {
                                "endpointUrl": "data/indexes",
                                "blackList": "^_.*$",
                                "createSearchChoice": true
                            },
                            "required": true,
                            "validators": [
                                {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 80,
                                    "errorMsg": "Length of index name should be between 1 and 80."
                                }
                            ]
                        },
                        {
                            "field": "page_size",
                            "label": "Page Size",
                            "help": "Maximum page size is 1000.",
                            "required": false,
                            "type": "text",
                            "validators": [
                                {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 8192,
                                    "errorMsg": "Max length of text input is 8192"
                                }
                            ]
                        },
                        {
                            "field": "from",
                            "label": "From",
                            "help": "Date in UTC (YYYY-MM-DDTHH:MM:SSZ), from which the events should be fetched. Default will be last 24 hours.",
                            "required": false,
                            "type": "text",
                            "validators": [
                                {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 8192,
                                    "errorMsg": "Max length of text input is 8192"
                                }
                            ]
                        },
                        {
                            "field": "to",
                            "label": "To",
                            "help": "Date in UTC (YYYY-MM-DDTHH:MM:SSZ), from which the events should be fetched. Default will be last 24 hours.",
                            "required": false,
                            "type": "text",
                            "validators": [
                                {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 8192,
                                    "errorMsg": "Max length of text input is 8192"
                                }
                            ]
                        }
                    ]
                },
                {
                    "name": "sophos_event_input",
                    "title": "Sophos Event Input",
                    "entity": [
                        {
                            "field": "name",
                            "label": "Name",
                            "type": "text",
                            "help": "Enter a unique name for the data input",
                            "required": true,
                            "validators": [
                                {
                                    "type": "regex",
                                    "pattern": "^[a-zA-Z]\\w*$",
                                    "errorMsg": "Input Name must start with a letter and followed by alphabetic letters, digits or underscores."
                                },
                                {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 100,
                                    "errorMsg": "Length of input name should be between 1 and 100"
                                }
                            ]
                        },
                        {
                            "field": "interval",
                            "label": "Interval",
                            "type": "text",
                            "required": true,
                            "help": "Time interval of input in seconds.",
                            "validators": [
                                {
                                    "type": "regex",
                                    "pattern": "^\\-[1-9]\\d*$|^\\d*$",
                                    "errorMsg": "Interval must be an integer."
                                }
                            ]
                        },
                        {
                            "field": "index",
                            "label": "Index",
                            "type": "singleSelect",
                            "defaultValue": "default",
                            "options": {
                                "endpointUrl": "data/indexes",
                                "blackList": "^_.*$",
                                "createSearchChoice": true
                            },
                            "required": true,
                            "validators": [
                                {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 80,
                                    "errorMsg": "Length of index name should be between 1 and 80."
                                }
                            ]
                        },
                        {
                            "field": "page_size",
                            "label": "Page Size",
                            "help": "Maximum page size is 1000.",
                            "required": false,
                            "type": "text",
                            "validators": [
                                {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 8192,
                                    "errorMsg": "Max length of text input is 8192"
                                }
                            ]
                        },
                        {
                            "field": "from",
                            "label": "From",
                            "help": "Date in UTC (YYYY-MM-DDTHH:MM:SSZ), from which the events should be fetched. Default will be last 24 hours.",
                            "required": false,
                            "type": "text",
                            "validators": [
                                {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 8192,
                                    "errorMsg": "Max length of text input is 8192"
                                }
                            ]
                        },
                        {
                            "field": "to",
                            "label": "To",
                            "help": "Date in UTC (YYYY-MM-DDTHH:MM:SSZ), from which the events should be fetched. Default will be last 24 hours.",
                            "required": false,
                            "type": "text",
                            "validators": [
                                {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 8192,
                                    "errorMsg": "Max length of text input is 8192"
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    }
}